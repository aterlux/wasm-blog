#pragma once

#include <stdint.h>

uint32_t levenshtein_2(const char *query, uint32_t query_len, const char *against, uint32_t against_len);

// WARNING: I was getting heap buffer overflows with this and nothing I did in JS could fix it.
// IDK what I was doing wrong.
#define MIN(x, y) ((x) < (y) ? (x) : (y))
uint32_t levenshtein_1(char *query, char *against);
