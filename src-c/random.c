#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "emcc.h"
#include "exit.h"

#include "fuzzy_match.h"
#include "levenshtein.h"



int EMSCRIPTEN_KEEPALIVE fake_add(int num1, int num2)
{
	if (num1 == 2) {
		num1 = 3;
	}
	return num1 * num1 + num2;
}

int EMSCRIPTEN_KEEPALIVE sum(int nums[], int len)
{
	int sum = 0;
	printf("len: %d\n", len);
	for (int lol = 0; lol != 10000; lol++) {
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++) {
				for (int k = 0; k < len; k++) {
					sum += nums[i];
				}
			}
		}
	}
	return sum;
}

int EMSCRIPTEN_KEEPALIVE sum2(int num1, int num2)
{
	int sum = 0;
	for (int lol = 0; lol != 1000; lol++) {
		sum += num1 + num2;
	}
	return sum;
}


int EMSCRIPTEN_KEEPALIVE printmychars(char *str, int len)
{
	printf("len: %d\n", len);
	for (int i = 0; i != len; ++i) {
		printf("char: %c\n", str[i]);
	}
	return 0;
}

typedef struct {
	int8_t asdf;
	int8_t qwer;
	int8_t intarr[];
} lol;

void EMSCRIPTEN_KEEPALIVE lol_struct(void *wtf)
{
	lol *mystruct = (lol *)wtf;
	printf("mystruct asdf: %d\n", mystruct->asdf);
	printf("mystruct qwer: %d\n", mystruct->qwer);
	printf("mystruct intarr[0]: %d\n", mystruct->intarr[0]);
	printf("mystruct intarr[1]: %d\n", mystruct->intarr[1]);
	printf("mystruct intarr[2]: %d\n", mystruct->intarr[2]);
	printf("mystruct intarr[3] !! : %d\n", mystruct->intarr[3]);
}


void EMSCRIPTEN_KEEPALIVE find_query_in_strings(char *query, char **findin, int num_findin, int32_t *res)
{
	if (num_findin < 1) {
		exit(CEXIT_QUERY_FILES_TOO_FEW_STRINGS);
	}

	for (int i = 0; i != num_findin; ++i) {
		// printf("query str: %s\n", strings[i]);
		res[i] = levenshtein_2(query, strlen(query), findin[i], strlen(findin[i]));
	}
}

// https://www.geeksforgeeks.org/bubble-sort/
// TODO: Use not bubble sort please
void sort_strings_by_distance(int32_t res_sorted_distances[], char **str_arr, int32_t arr_count)
{
	int32_t i = 0;
	int32_t j = 0;
	bool swapped = false;
	char *temp_str = "";
	for (i = 0; i < arr_count - 1; i++) {
		swapped = false;
		for (j = 0; j < arr_count - i - 1; j++) {
			if (res_sorted_distances[j] < res_sorted_distances[j + 1]) {
				// swap distance ints and strings
				int32_t temp_dist = res_sorted_distances[j];
				res_sorted_distances[j] = res_sorted_distances[j + 1];
				res_sorted_distances[j + 1] = temp_dist;

				temp_str = str_arr[j];
				str_arr[j] = str_arr[j + 1];
				str_arr[j + 1] = temp_str;
				swapped = true;
			}
		}

		// If no two elements were swapped by inner loop,
		// then break
		if (swapped == false) {
			break;
		}
	}
}

// Find Lavenshtein distance of all findin strings,
// then re-arrange them based on their distance
void EMSCRIPTEN_KEEPALIVE order_strings_by_query_match(
	char *query, char **findin, int32_t num_findin, int32_t *res_sorted_distances)
{
	for (int32_t i = 0; i != num_findin; ++i) {
		// res_sorted_distances[i] = levenshtein_2(query, strlen(query), findin[i], strlen(findin[i]));
		res_sorted_distances[i] = fuzzy_match(query, findin[i]);
	}

	sort_strings_by_distance(res_sorted_distances, findin, num_findin);
}
