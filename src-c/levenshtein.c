
#include "levenshtein.h"
#include <stdlib.h>
#include <string.h>

// See: https://github.com/wooorm/levenshtein.c/blob/main/levenshtein.c
uint32_t levenshtein_2(const char *query, const uint32_t query_len, const char *against, const uint32_t against_len)
{
	// Shortcut optimizations / degenerate cases.
	if (query == against) {
		return 0;
	}

	if (query_len == 0) {
		return against_len;
	}

	if (against_len == 0) {
		return query_len;
	}

	if (query_len > against_len) {
		return UINT32_MAX;
	}

	uint32_t *cache = calloc(query_len, sizeof(uint32_t));
	uint32_t q_index = 0;
	uint32_t a_index = 0;
	uint32_t q_distance = 0;;
	uint32_t a_distance = 0;
	uint32_t result = 0;
	char code;

	// initialize the vector.
	while (q_index < query_len) {
		cache[q_index] = q_index + 1;
		q_index++;
	}

	// Loop.
	while (a_index < against_len) {
		code = against[a_index];
		result = q_distance = a_index++;
		q_index = SIZE_MAX;

		while (++q_index < query_len) {
			a_distance = code == query[q_index] ? q_distance : q_distance + 1;
			q_distance = cache[q_index];

			cache[q_index] = result = q_distance > result ? a_distance > result ? result + 1 : a_distance
				: a_distance > q_distance				  ? q_distance + 1
													  : a_distance;
		}
	}

	free(cache);

	return result;
}

uint32_t levenshtein_1(char *query, char *against)
{
	int distance[100][100];


	int i = 0;;
	int j = 0;;
	int query_len = 0;
	int against_len = 0;
	int temp = 0;
	int tracker = 0;

	query_len = strlen(query);

	against_len = strlen(against);

	// Skip calculations for empty queries
	// and for queries longer than comparison string.
	// This is for optimisation and as a design choice
	//
	// Empty queries will show all files
	if (query_len == 0) {
		return 0;
	}
	if (query_len > against_len) {
		return UINT32_MAX;
	}

	for (i = 0; i <= query_len; i++) {
		distance[0][i] = i;
	}

	for (j = 0; j <= against_len; j++) {
		distance[j][0] = j;
	}

	for (j = 1; j <= query_len; j++) {

		for (i = 1; i <= against_len; i++) {

			if (query[i - 1] == against[j - 1]) {

				tracker = 0;
			}
			else {

				tracker = 1;
			}

			temp = MIN((distance[i - 1][j] + 1), (distance[i][j - 1] + 1));

			distance[i][j] = MIN(temp, (distance[i - 1][j - 1] + tracker));
		}
	}

	return distance[against_len][query_len];
}

