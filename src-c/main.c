
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "lib/md4c-html.h"
#include "emcc.h"
#include "exit.h"



// "Wrapped" here so they're easily exported to JS
void EMSCRIPTEN_KEEPALIVE *z_malloc(size_t size)
{
	return malloc(size);
}

void EMSCRIPTEN_KEEPALIVE z_free(void *ptr)
{
	free(ptr);
}


/* Global options. */
// static unsigned parser_flags = 0;
#ifndef MD4C_USE_ASCII
// static unsigned renderer_flags = MD_HTML_FLAG_DEBUG | MD_HTML_FLAG_SKIP_UTF8_BOM;
#else
static unsigned renderer_flags = MD_HTML_FLAG_DEBUG;
#endif
// static int want_fullhtml = 0;
// static int want_xhtml = 0;
// static int want_stat = 0;
// static int want_replay_fuzz = 0;

// static const char *html_title = NULL;
// static const char *css_path = NULL;


/*********************************
 ***  Simple grow-able buffer  ***
 *********************************/

/* We render to a memory buffer instead of directly outputting the rendered
 * documents, as this allows using this utility for evaluating performance
 * of MD4C (--stat option). This allows us to measure just time of the parser,
 * without the I/O.
 */

struct membuffer {
	char *data;
	size_t asize;
	size_t size;
};

// static void membuf_init(struct membuffer *buf, MD_SIZE new_asize)
// {
// 	buf->size = 0;
// 	buf->asize = new_asize;
// 	buf->data = malloc(buf->asize);
// 	if (buf->data == NULL) {
// 		fprintf(stderr, "membuf_init: malloc() failed.\n");
// 		exit(1);
// 	}
// }
//
// static void membuf_fini(struct membuffer *buf)
// {
// 	if (buf->data) {
// 		free(buf->data);
// 	}
// }

static void membuf_grow(struct membuffer *buf, size_t new_asize)
{
	char *tmp = realloc(buf->data, new_asize);
	if (tmp == NULL) {
		free(buf->data);
		fprintf(stderr, "membuf_grow: realloc() failed.\n");
		exit(CEXIT_membuf_grow);
	}
	buf->data = tmp;
	buf->asize = new_asize;
}

static void membuf_append(struct membuffer *buf, const char *data, MD_SIZE size)
{
	if (buf->asize < buf->size + size) {
		membuf_grow(buf, buf->size + buf->size / 2 + size);
	}
	memcpy(buf->data + buf->size, data, size);
	buf->size += size;
}


/**********************
 ***  Main program  ***
 **********************/

static void process_output(const MD_CHAR *text, MD_SIZE size, void *userdata)
{
	membuf_append((struct membuffer *)userdata, text, size);
}



// int parseme()
int EMSCRIPTEN_KEEPALIVE parseme(void)
{
	char input_md[]
		= "# Public\n I want to have this in a relatively transferrable data format.\n Ideally I want to have some "
		  "sort of fuzzy finding on a website view.\n ## Info-ish\n Cars\n - BMW E46\n - Engineering bits in general\n "
		  "## \"blog\"-y personal bits\n Anime\n - Anime girl colours\n - Anime quotes\n - Anime to watch (?)\n - "
		  "Manga to read\n Biking\n - Parts\n - Tips\n - etc.\n Media\n - Book list (fiction etc., not informative)\n "
		  "- Music\n - Films\n Diet\n - Keto\n - Omega3/Omega6\n - intermittent fasting\n Quotes\n - Practical guide "
		  "to evil\n - etc.\n ## Blog actual personal\n - Ideas\n - Personal philosophies\n - Goals in life\n ## "
		  "Informational\n Take from `_Actual Documents/_Normal/Informational`.\n Create some sort of "
		  "\"reference-base\" for all PDFs, maybe with downloads on login.\n ## Programming and web dev\n Basically "
		  "pull in `web-tutorials-and-snippets/notes`, `dev-tooling/cpp`, and some other misc. bits\n Generally:\n - "
		  "C++ \"guidelines\"\n - C++ knowledge references -- books, videos, even stackoverflow links.\n - "
		  "JavaScript/TypeScript guidelines\n - JavaScript/TypeScript knowledge references\n - Notes on fonts, "
		  "colours, etc.\n - Opinions on software dev, web dev, tech stacks, etc.\n - Data formats (JSON, protobuf, "
		  "capnproto, flatbuffers, msgpack)\n - Guides for how to do stuff\n - Maybe somehow automatically import "
		  "`smol-code/cpp-snippets` GitLab repo and create HTML sites from that???\n - JWT vs session\n - HTTP "
		  "Content-Type headers and notes on protobuf\n Useful libraries and tools:\n - C++ libs\n - JS libs\n - Tools "
		  "such as that Golang SQL mock data generator. So many things can go here\n - Dbeaver\n - etc. etc.\n \"My "
		  "development environment\":\n - Linux/WSL\n - Neovim\n - etc.\n Links to my code repos and hosted websites\n "
		  "- aterlux\n - web-tech-degree\n - smol-code\n - configs-setups\n - etc.\n ## Work\n Uni\n - Design work\n - "
		  "Written work (strip for privacy)\n - Organise GitLab links\n - Host built websites\n ## "
		  "Photography/pictures\n - Computer stuff\n - Car stuff\n - Cat and animals\n - Nature -- flowers, mountains, "
		  "etc.\n - Macro photography\n ## Cool programs and stuff\n Unrelated to programming\n Websites:\n - "
		  "https://z0r.de\n - Cobalt tools (Twitter)\n Windows:\n - foobar2000\n - MSI Afterburner\n - ZenTimings / "
		  "Ryzen Timings Checker\n - Prime05 etc. etc.\n - Greenshot\n Linux:\n - Flameshot\n - etc.\n # Private\n - "
		  "CVs\n# Public\n I want to have this in a relatively transferrable data format.\n Ideally I want to have "
		  "some "
		  "sort of fuzzy finding on a website view.\n ## Info-ish\n Cars\n - BMW E46\n - Engineering bits in general\n "
		  "## \"blog\"-y personal bits\n Anime\n - Anime girl colours\n - Anime quotes\n - Anime to watch (?)\n - "
		  "Manga to read\n Biking\n - Parts\n - Tips\n - etc.\n Media\n - Book list (fiction etc., not informative)\n "
		  "- Music\n - Films\n Diet\n - Keto\n - Omega3/Omega6\n - intermittent fasting\n Quotes\n - Practical guide "
		  "to evil\n - etc.\n ## Blog actual personal\n - Ideas\n - Personal philosophies\n - Goals in life\n ## "
		  "Informational\n Take from `_Actual Documents/_Normal/Informational`.\n Create some sort of "
		  "\"reference-base\" for all PDFs, maybe with downloads on login.\n ## Programming and web dev\n Basically "
		  "pull in `web-tutorials-and-snippets/notes`, `dev-tooling/cpp`, and some other misc. bits\n Generally:\n - "
		  "C++ \"guidelines\"\n - C++ knowledge references -- books, videos, even stackoverflow links.\n - "
		  "JavaScript/TypeScript guidelines\n - JavaScript/TypeScript knowledge references\n - Notes on fonts, "
		  "colours, etc.\n - Opinions on software dev, web dev, tech stacks, etc.\n - Data formats (JSON, protobuf, "
		  "capnproto, flatbuffers, msgpack)\n - Guides for how to do stuff\n - Maybe somehow automatically import "
		  "`smol-code/cpp-snippets` GitLab repo and create HTML sites from that???\n - JWT vs session\n - HTTP "
		  "Content-Type headers and notes on protobuf\n Useful libraries and tools:\n - C++ libs\n - JS libs\n - Tools "
		  "such as that Golang SQL mock data generator. So many things can go here\n - Dbeaver\n - etc. etc.\n \"My "
		  "development environment\":\n - Linux/WSL\n - Neovim\n - etc.\n Links to my code repos and hosted websites\n "
		  "- aterlux\n - web-tech-degree\n - smol-code\n - configs-setups\n - etc.\n ## Work\n Uni\n - Design work\n - "
		  "Written work (strip for privacy)\n - Organise GitLab links\n - Host built websites\n ## "
		  "Photography/pictures\n - Computer stuff\n - Car stuff\n - Cat and animals\n - Nature -- flowers, mountains, "
		  "etc.\n - Macro photography\n ## Cool programs and stuff\n Unrelated to programming\n Websites:\n - "
		  "https://z0r.de\n - Cobalt tools (Twitter)\n Windows:\n - foobar2000\n - MSI Afterburner\n - ZenTimings / "
		  "Ryzen Timings Checker\n - Prime05 etc. etc.\n - Greenshot\n Linux:\n - Flameshot\n - etc.\n # Private\n - "
		  "CVs\n# h1 Heading 8-\n ## h2 Heading\n ### h3 Heading\n #### h4 Heading\n ##### h5 Heading\n ###### h6 "
		  "Heading\n ## Horizontal Rules\n ___\n ---\n ***\n ## Typographic replacements\n Enable typographer option "
		  "to see result.\n (c) (C) (r) (R) (tm) (TM) (p) (P) +-\n test.. test... test..... test?..... test!....\n "
		  "!!!!!! ???? ,,  -- ---\n \"Smartypants, double quotes\" and 'single quotes'\n ## Emphasis\n **This is bold "
		  "text**\n __This is bold text__\n *This is italic text*\n _This is italic text_\n ~~Strikethrough~~\n ## "
		  "Blockquotes\n > Blockquotes can also be nested...\n >> ...by using additional greater-than signs right next "
		  "to each other...\n > > > ...or with spaces between arrows.\n ## Lists\n Unordered\n + Create a list by "
		  "starting a line with `+`, `-`, or `*`\n + Sub-lists are made by indenting 2 spaces:\n   - Marker character "
		  "change forces new list start:\n     * Ac tristique libero volutpat at\n     + Facilisis in pretium nisl "
		  "aliquet\n     - Nulla volutpat aliquam velit\n + Very easy!\n Ordered\n 1. Lorem ipsum dolor sit amet\n 2. "
		  "Consectetur adipiscing elit\n 3. Integer molestie lorem at massa\n 1. You can use sequential numbers...\n "
		  "1. ...or keep all the numbers as `1.`\n Start numbering with offset:\n 57. foo\n 1. bar\n ## Code\n Inline "
		  "`code`\n Indented code\n     // Some comments\n     line 1 of code\n     line 2 of code\n     line 3 of "
		  "code\n Block code \"fences\"\n ```\n Sample text here...\n ```\n Syntax highlighting\n ``` js\n var foo = "
		  "function (bar) {\n   return bar++;\n };\n console.log(foo(5));\n ```\n ## Tables\n | Option | Description "
		  "|\n | ------ | ----------- |\n | data   | path to data files to supply the data that will be passed into "
		  "templates. |\n | engine | engine to be used for processing templates. Handlebars is the default. |\n | ext  "
		  "  | extension to be used for dest files. |\n Right aligned columns\n | Option | Description |\n | ------:| "
		  "-----------:|\n | data   | path to data files to supply the data that will be passed into templates. |\n | "
		  "engine | engine to be used for processing templates. Handlebars is the default. |\n | ext    | extension to "
		  "be used for dest files. |\n ## Links\n [link text](http://dev.nodeca.com)\n [link with "
		  "title](http://nodeca.github.io/pica/demo/ \"title text!\")\n Autoconverted link "
		  "https://github.com/nodeca/pica (enable linkify to see)\n ## Images\n "
		  "![Minion](https://octodex.github.com/images/minion.png)\n "
		  "![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg \"The Stormtroopocat\")\n Like "
		  "links, Images also have a footnote style syntax\n ![Alt text][id]\n With a reference later in the document "
		  "defining the URL location:\n [id]: https://octodex.github.com/images/dojocat.jpg  \"The Dojocat\"\n ## "
		  "Plugins\n The killer feature of `markdown-it` is very effective support of\n [syntax "
		  "plugins](https://www.npmjs.org/browse/keyword/markdown-it-plugin).\n ### "
		  "[Emojies](https://github.com/markdown-it/markdown-it-emoji)\n > Classic markup: :wink: :cry: :laughing: "
		  ":yum:\n >\n > Shortcuts (emoticons): :-) :-( 8-) ;)\n see [how to change "
		  "output](https://github.com/markdown-it/markdown-it-emoji#change-output) with twemoji.\n ### "
		  "[Subscript](https://github.com/markdown-it/markdown-it-sub) / "
		  "[Superscript](https://github.com/markdown-it/markdown-it-sup)\n - 19^th^\n - H~2~O\n ### "
		  "[\\<ins>](https://github.com/markdown-it/markdown-it-ins)\n ++Inserted text++\n ### "
		  "[\\<mark>](https://github.com/markdown-it/markdown-it-mark)\n ==Marked text==\n ### "
		  "[Footnotes](https://github.com/markdown-it/markdown-it-footnote)\n Footnote 1 link[^first].\n Footnote 2 "
		  "link[^second].\n Inline footnote^[Text of inline footnote] definition.\n Duplicated footnote "
		  "reference[^second].\n [^first]: Footnote **can have markup**\n     and multiple paragraphs.\n [^second]: "
		  "Footnote text.\n ### [Definition lists](https://github.com/markdown-it/markdown-it-deflist)\n Term 1\n :   "
		  "Definition 1\n with lazy continuation.\n Term 2 with *inline markup*\n :   Definition 2\n         { some "
		  "code, part of Definition 2 }\n     Third paragraph of definition 2.\n _Compact style:_\n Term 1\n   ~ "
		  "Definition 1\n Term 2\n   ~ Definition 2a\n   ~ Definition 2b\n ### "
		  "[Abbreviations](https://github.com/markdown-it/markdown-it-abbr)\n This is HTML abbreviation example.\n It "
		  "converts \"HTML\", but keep intact partial entries like \"xxxHTMLyyy\" and so on.\n *[HTML]: Hyper Text "
		  "Markup Language\n ### [Custom containers](https://github.com/markdown-it/markdown-it-container)\n ::: "
		  "warning\n *here be dragons*\n :::\n ";

	float time_start = (float)clock() / CLOCKS_PER_SEC;
	struct membuffer buf_out = { 0 };

	md_html(input_md, strlen(input_md), process_output, &buf_out, 0, 0);

	float time_end = (float)clock() / CLOCKS_PER_SEC;
	printf("completed???\n");
	printf("time taken: %f\n", time_end - time_start);
	printf("buffer data: %s\n", buf_out.data);
	return 42;
}


// int main(void)
// {
// 	return 0;
// }
