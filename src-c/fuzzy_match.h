#pragma once

#include <stdint.h>

// See: https://github.com/philj56/fuzzy-match?tab=readme-ov-file
int32_t fuzzy_match(const char *restrict pattern, const char *restrict str);
