module.exports = {
	// This file only handles the rules/settings
	"rules": {

		// Allow the use of ts-ignore for one liners
		// "@typescript-eslint/ban-ts-ignore": "warn",
		"@typescript-eslint/ban-ts-comment": "warn",

		// Turn off requirement for jsdoc
		"require-jsdoc": [
			"error", {
				"require": {
					"FunctionDeclaration": false,
					"MethodDefinition": false,
					"ClassDeclaration": false,
					"ArrowFunctionExpression": false,
					"FunctionExpression": false,
				},
			},
		],

		"simple-import-sort/imports": "error",
		"simple-import-sort/exports": "error",

		// Disable warning about PascalCase functions
		// requiring to be constructors.
		// Only done for WASM/emcc UTF8ToString etc. functions
		"new-cap": [
			"error",
			{
				"capIsNew": false
			}
		],

		// Max line length
		"max-len": [
			"error",
			{
				"code": 256,
			},
		],

		"max-lines-per-function": [
			"warn",
			{
				"max": 9999,
				"skipBlankLines": true,
				"skipComments": true,
			},
		],

		"no-multiple-empty-lines": [
			"warn",
			{
				"max": 5,
				"maxEOF": 0,
			},
		],

		// Require line comments to be above the code, not in-line
		"line-comment-position": [
			"error",
			"above",
		],

		// Use // on multi-line comments
		"multiline-comment-style": [
			"error",
			"separate-lines",
		],

		// Brace style for if/else statements etc.
		"brace-style": [
			"error",
			"stroustrup",
		],

		"quotes": [
			"error",
			"double",
		],

		"camelcase": [
			"off"
		],

		// Require semicolons where they are required
		"semi": [
			"error",
			"always",
		],

		// Use tab indentation
		"indent": [
			"error",
			"tab",
		],

		// No tabs also stops ESLint from screaming about tabs in comments
		"no-tabs": 0,

		// Use \n instead of \r\n
		"linebreak-style": [
			"error",
			"unix",
		],

		// Allow classes to be used above/below each other... JavaScript is hoisted
		// and I'm making the exception for classes, though we'll probably also have exceptions
		// for functions
		"no-use-before-define": [
			0, {
				"classes": false,
			},
		],

		"@typescript-eslint/no-use-before-define": [
			0, {
				"classes": false,
			},
		],

		// Always require === and !== instead of == and !=
		"eqeqeq": [
			"error",
			"always",
		],

		// Must throw an Error object and not re-cast to a string literal
		"no-throw-literal": [
			"error",
		],

		"ban/ban": [
			1,
			{ "name": ["*", "forEach"], "message": "Use a for loop" },
			{ "name": ["*", "map"], "message": "Use a for loop" },
			{ "name": ["*", "some"], "message": "Use a for loop" },
			{ "name": ["*", "reduce"], "message": "Use a for loop" },
			{ "name": ["*", "filter"], "message": "Use a for loop" },
		],

		// Warn about unused stuff; do not error, and do not autofix, as often
		// stuff may be written that will be used later etc.
		// https://eslint.org/docs/rules/no-unused-labels
		"no-unused-labels": [
			"warn",
		],

		"no-unused-vars": [
			"off",
		],

		// See: https://github.com/typescript-eslint/typescript-eslint/issues/1197
		// This needs to be the TypeScript rule so that it can understand stuff like
		// enums, as they otherwise appears as "unused"
		// https://eslint.org/docs/rules/no-unused-vars
		"@typescript-eslint/no-unused-vars": [
			"warn",
		],

		// Require a new line on end of file
		"eol-last": [
			"error",
			"always",
		],
	},
};
