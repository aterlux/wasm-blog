#!/bin/bash

echo "---------- running clang-format ----------"
find src-c -name '*.h' -o -name '*.c' -print | xargs clang-format -i

echo "---------- running dos2unix ----------"
find . -type d \( -path ./third_party -o -path ./dist -o -path ./.git \) -prune -o -name '*.*' -print0 | xargs -0 dos2unix -q
