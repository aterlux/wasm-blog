module.exports = {
	// -------------------------------------------------------------------------
	// NOTE: Adapted from my SolidJS project... which is adapted from a uni project of mine
	// - https://gitlab.com/aterlux/solidjs-home/-/blob/master/.eslintrc.js?ref_type=heads
	// - https://gitlab.com/web-tech-degree/ria-gamestart/-/blob/master/gameinc-g/.eslintrc.js
	// ... lol
	// -------------------------------------------------------------------------
	"env": {
		"es2020": true,
		"node": true,
	},
	"extends": [
		"eslint:recommended",
		"google",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended",
		// https://typescript-eslint.io/linting/configs/#recommended-requiring-type-checking
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		// Import ruleset
		"./.eslint-global-config.cjs",
		"./.eslint-ts-config.cjs",
	],
	"parserOptions": {
		"parser": "@typescript-eslint/parser",
		"ecmaFeatures": {
			"jsx": true,
		},
		"ecmaVersion": 2020,
		"sourceType": "module",
		"project": "./tsconfig.json",
	},
	"plugins": [
		"@typescript-eslint",
		"solid",
		"ban",
		"simple-import-sort"
	],
};
