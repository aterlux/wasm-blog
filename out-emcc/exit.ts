

// This is based on /src-c/exit.h
// When an `exit()` is called, it will be
// using one of those unique numbers.
// This might ever so slightly aid debugging
// if `exit()` is used commonly.


// eslint-disable-next-line @typescript-eslint/naming-convention
export const CEXIT_STATUSES = {
	1: "default",
	2: "[main.c] [membuf_grow()] failed realloc()",
} as const;
