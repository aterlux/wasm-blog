import {t_cptr, tc_module} from "../../out-emcc/wasm";



// ------------------------------
// Explanation
//
// These are helpers for allocating memory
// to pass to C.
//
// Take note and care around which fields
// you pass to C. They are annotated.
//
// All of these must be freed with a call
// to C's `free()`
// ------------------------------




// C: char* string
export type tc_string = {
	// Pass to C
	ptr_buf: t_cptr,
	// Pass to C
	strlen: number,
}

// Convert JS string to UTF8 array and allocate+copy to C memory.
//
// NOTE: Must be freed with `c._z_free();`
//
// Allocates one extra for "null" but returns original strlen
// as C's `strlen()` excludes the terminating null character
export const alloc_copy_string = (c: tc_module, str: string): tc_string => {
	const strlen = c.lengthBytesUTF8(str) + 1;
	const ptr_buf = c._z_malloc(strlen);
	c.stringToUTF8(str, ptr_buf, strlen);
	return {ptr_buf, strlen: strlen - 1};
};



// C: char** strings
export type tc_str_arr = {
	// malloc() pointer which holds cptr_strings
	// Pass to C
	ptr_container: t_cptr,
	// malloc() UTF8 strings.
	// Don't pass to C
	ptr_strings: t_cptr[],
	// Pass to C
	num_strings: number,
}

// Allocate and initialises a string[] for C,
// with each string (char *) having the minimum required length.
//
// See: https://stackoverflow.com/a/70267473/13310905
export const alloc_copy_strarr = (c: tc_module, strings: string[]): tc_str_arr => {
	const a: tc_str_arr = {
		ptr_container: 0,
		ptr_strings: [],
		num_strings: 0,
	};

	for (let i = 0; i !== strings.length; ++i) {
		const cstr_new = alloc_copy_string(c, strings[i]);
		a.ptr_strings.push(cstr_new.ptr_buf);
	}

	a.ptr_container = c._z_malloc(a.ptr_strings.length * 4);

	// set string values
	for (let i = 0; i !== a.ptr_strings.length; ++i) {
		c.setValue(a.ptr_container + i * 4, a.ptr_strings[i], "i8*");
	}

	a.num_strings = strings.length;

	return a;
};

// Allocates an uninitialised string[] for C,
// with each string (char *) having the provided length.
//
// WARNING: May or may not be broken
export const alloc_empty_strarr = (c: tc_module, string_count: number, strlen: number): tc_str_arr => {
	const a: tc_str_arr = {
		ptr_container: 0,
		ptr_strings: [],
		num_strings: 0,
	};

	for (let i = 0; i !== string_count; ++i) {
		const ptr_buf = c._z_malloc(strlen);
		a.ptr_strings.push(ptr_buf);
	}

	a.ptr_container = c._z_malloc(a.ptr_strings.length * 4);

	a.num_strings = strlen;

	return a;
};

// Free the buffers allocated by alloc_copy_strarr
export const free_strarr = (c: tc_module, str_arr: tc_str_arr): void => {
	for (let i = 0; i !== str_arr.ptr_strings.length; ++i) {
		c._z_free(str_arr.ptr_strings[i]);
	}

	c._z_free(str_arr.ptr_container);
};

export type t_strlen = {
	js: number,
	utf8: number
}

// Returns the strlen of the longest string in the array.
//
export const find_longest_strlen = (c: tc_module, strings: string[]): t_strlen => {
	// let longest_strlen = c.lengthBytesUTF8(strings[0]);
	let longest_strlen = strings[0].length;
	let longest_strlen_idx = 0;

	for (let i = 1; i !== strings.length; ++i) {
		// const this_len = c.lengthBytesUTF8(strings[i]);
		const this_len = strings[i].length;
		if (this_len > longest_strlen) {
			longest_strlen = this_len;
			longest_strlen_idx = i;
		}
	}

	return {
		js: longest_strlen,
		utf8: c.lengthBytesUTF8(strings[longest_strlen_idx]),
	};
};

// type tc_struct_lol = {
// 	asdf: number,
// 	qwer: number,
// 	intarr: number[]
// };


// type t_dataview_values = Record<string, {
// 	// if 1, doesn't use array
// 	len: number
// }>;
//
// export const alloc_struct_u32 = (c: t_c_module, values: t_dataview_values) => {
// 	let buf_size = 0;
// 	const address_offsets: Record<string, number> = {};
//
// 	const keys = Object.keys(values);
//
// 	for (const k of keys) {
// 		if (values[k].len === 0) {
// 			throw new Error("alloc_struct_u32 value must be at least len 1");
// 		}
// 		address_offsets[k] = buf_size;
// 		buf_size += values[k].len * 4;
// 	}
//
// 	const buf = c._z_malloc(buf_size);
// 	console.log("buf size: ", buf_size);
// 	console.log("offsets: ", address_offsets);
//
// 	const view_u32 = new DataView(c.HEAPU32.buffer);
//
// 	const set = (name: string, val: number) => {
// 		view_u32.setUint32(buf as number + address_offsets[name], val);
// 	};
//
// 	return {buf, set};
// };
