import {CEXIT_STATUSES} from "../../out-emcc/exit";
import c_module from "../../out-emcc/wasm";

export const init_wasm = async () => {
	console.warn(
		"If you see an \"Uncaught\" exception with a name of \"ExitStatus\",\n" +
		"refer to this for the status codes\n",
		CEXIT_STATUSES,
	);

	return c_module({
		// Override the JS print function called via printf();
		print: (text: string) => {
			console.debug("printf:", text);
		},
	});
};
