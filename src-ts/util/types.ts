import {Accessor} from "solid-js";

// See: https://stackoverflow.com/a/39495173/13310905
type t_intrage_enumerate<N extends number, Acc extends number[] = []> = Acc["length"] extends N
  ? Acc[number]
    : t_intrage_enumerate<N, [...Acc, Acc["length"]]>;

// Limits range of numbers of type
//
// NOTE: Maximum value is T_max_neg1 - 1
// So t_int_range<20, 100)> allows 20 to 99
//
// NOTE: Does NOT work with negatives
export type t_int_range<T_min extends number, T_max_neg1 extends number> = Exclude<t_intrage_enumerate<T_max_neg1>, t_intrage_enumerate<T_min>>



export const C_INT32_MIN = -2147483648;



// t_sig types define types used with createSignal(0

export type t_sig_settings = {
    index_file: string;
    use_index_file: boolean;
    markdown_location: string;
}
