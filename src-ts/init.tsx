import "./style.css";

import {createSignal} from "solid-js";
import {render} from "solid-js/web";

import {tc_module} from "../out-emcc/wasm";
import Main from "./ui/main";
import {init_wasm} from "./util/init_wasm";



// This will give dev some feedback if WASM is compiling,
// as there's a delay. This only happens on WASM compile.
if (import.meta.hot) {
	import.meta.hot.on("my:wasm_wait", () => {
		console.error("Compiling WASM, please wait");
		document.body.style.backgroundColor = "#666";
	});
}


render(() => {
	const [c, set_c] = createSignal<tc_module | undefined>(undefined);
	init_wasm()
		.then((loaded_wasm) => set_c(loaded_wasm))
		.catch((err) => console.error("WASM failed to load or something else happened", err));

	return (
		<>
			{c() !== undefined ?
				<Main c={c() as tc_module} /> :
				<p>loading...</p>

			}
		</>
	);
}, document.body);


// IIFE
// void (() => main())();
