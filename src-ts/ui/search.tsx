import "./search.css";

import {setSourceMapsEnabled} from "process";
import {Accessor, createEffect, createSignal, JSXElement, onCleanup, onMount, Setter} from "solid-js";

import {t_cptr, tc_module} from "../../out-emcc/wasm";
import {alloc_copy_strarr, alloc_copy_string, alloc_empty_strarr, find_longest_strlen, free_strarr, t_strlen, tc_str_arr, tc_string} from "../util/mem-interop";
import {C_INT32_MIN} from "../util/types";
import {test_query_strings} from "./test_strings";



// eslint-disable-next-line @typescript-eslint/naming-convention
type t_input_event = InputEvent & { target: HTMLInputElement, currentTarget: HTMLInputElement };

type t_sorted_distances = {
	// Searchable strings
	strings: string[]
	// Levenshtein distance
	distances: Int32Array
	// TODO: When adding ability for user to search
	// by heading/content, the original file name
	// needs to be tracked and sorted with this.
}

export default (props: { c: tc_module, files: Accessor<string[]>, set_current_file: Setter<string> }): JSXElement => {
	let ref_list: HTMLDivElement | undefined = undefined;

	// Tracks input value so it can be reset when props.files() changes
	const [file_search_term, set_file_search_term] = createSignal("");
	const [search_open, set_search_open] = createSignal(true);
	const [sorted, set_sorted] = createSignal<t_sorted_distances>({
		strings: props.files(),
		distances: new Int32Array(props.files().length),
	});
	// tracks where the user is with ArrowUp/ArrowDown
	const [selection_cursor, set_selection_cursor] = createSignal(0);

	// All these buffers are reused.
	// If input files change, these are re-allocated and re-initialised
	let c_findin: tc_str_arr = {
		ptr_container: 0,
		ptr_strings: [],
		num_strings: 0,
	};
	// char[files.length][strlen of longest str]
	let longest_strlen: t_strlen = {js: 0, utf8: 0};
	const c_res_sorted: tc_str_arr = {
		ptr_container: 0,
		ptr_strings: [],
		num_strings: 0,
	};
	// There will be as many distances as input files
	let cptr_distances: t_cptr = 0;

	const cleanup_malloc = () => {
		free_strarr(props.c, c_findin);
		free_strarr(props.c, c_res_sorted);
		props.c._z_free(cptr_distances);
	};

	// Runs on load and when props.files() change
	createEffect(() => {
		// if 0, this is first load
		if (c_findin.ptr_container !== 0) {
			set_file_search_term("");
			// Need to update this as it won't update until user types
			set_sorted({
				strings: props.files(),
				distances: new Int32Array(props.files().length),
			});
			cleanup_malloc();
		}

		c_findin = alloc_copy_strarr(props.c, props.files());
		// char[files.length][strlen of longest str]
		longest_strlen = find_longest_strlen(props.c, props.files());
		console.log("lognest strlen", longest_strlen);
		// There will be as many distances as input files
		cptr_distances = props.c._z_malloc(props.files().length * 4);
		props.c.HEAP32.set([], cptr_distances as number);
	});

	const search_input = (event: t_input_event) => {
		// UX: Open searh
		set_search_open(true);
		set_file_search_term(event.currentTarget.value);

		// Similar optimisation exists in C, but here this will
		// prevent re-render if user is spamming input past max strlen.
		//
		// UX: This also keeps the last match in the results, but:
		// NOTE: The last match currently isn't great because
		// only the longest strings will pop up as results,
		// due to the length conditional in C.
		if (event.currentTarget.value.length > longest_strlen.js) {
			return;
		}

		const c_query = alloc_copy_string(props.c, event.currentTarget.value);

		props.c._order_strings_by_query_match(
			c_query.ptr_buf,
			c_findin.ptr_container,
			c_findin.num_strings,
			cptr_distances,
		);

		const strings_int32 = props.c.HEAP32.subarray(
			c_findin.ptr_container / 4,
			c_findin.ptr_container / 4 + props.files().length,
		);

		const strings: string[] = [];
		for (let i = 0; i !== props.files().length; ++i) {
			strings[i] = props.c.UTF8ToString(strings_int32[i]);
		}

		set_sorted({
			strings: strings,
			distances: props.c.HEAP32.subarray(
				cptr_distances / 4,
				cptr_distances / 4 + props.files().length,
			),
		});

		// For UX and simplicity, reset cursor
		set_selection_cursor(0);

		props.c._z_free(c_query.ptr_buf);
	};

	const goto_selection = (file: string) => {
		// console.log("going to: ", sorted().strings[selection_cursor()]);
		console.log("going to: ", file);
		set_search_open(false);
		props.set_current_file(file);
	};

	// LMB will not select, just go to
	const lmb_select_file = (click: MouseEvent) => {
		if (click.target === null) {
			return;
		}
		const next_el = click.target as HTMLElement;
		const selection_idx = parseInt(next_el.dataset.selectionIndex || "0");
		goto_selection(sorted().strings[selection_idx]);
	};


	const select_list_keydown = (key: KeyboardEvent) => {
		if (ref_list === undefined) {
			return;
		}

		const last_el = ref_list.children[selection_cursor()];

		// Steals default because we don't
		// want the user to lose their text cursor
		// (Pressing ArrowUp is equivalent to Home, ArrowDown to End)
		//
		// Note that adding/removing class here
		// is more performant than changing the class
		// in `render_sorted()` and re-rendering
		switch (key.key) {
		case "ArrowDown": {
			key.preventDefault();
			let next_idx = selection_cursor() + 1;
			// wrap to first
			if (next_idx === props.files().length) {
				next_idx = 0;
			}

			const next_el = ref_list.children[next_idx];
			last_el?.classList.remove("selected");
			next_el.classList.add("selected");
			next_el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
			set_selection_cursor(next_idx);
			break;
		}
		case "ArrowUp": {
			key.preventDefault();
			let next_idx = selection_cursor() - 1;
			// wrap to last
			if (next_idx < 0) {
				next_idx = props.files().length - 1;
			}

			const next_el = ref_list.children[next_idx];
			last_el?.classList.remove("selected");
			next_el.classList.add("selected");
			next_el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
			set_selection_cursor(next_idx);
			break;
		}
		default:
			return;
		}
	};

	const render_sorted = (): JSXElement => {
		const elements: JSXElement[] = [];
		// data-selection-index is used only for getting the index
		// based on LMB mouse click.

		// No results
		if (sorted().distances[0] === C_INT32_MIN) {
			return <></>;
		}

		// First element always selected on search query change
		elements.push(
			<p
				class="search-item selected"
				onClick={lmb_select_file}
				data-selection-index={0}
			>
				{sorted().strings[0]} : {sorted().distances[0]}
			</p>,
		);

		// TODO: Add user-controlled threshold here
		// or maybe do it by changing a class (not in this func)
		// instead of re-rendering
		for (let i = 1; i !== sorted().distances.length; ++i) {
			// Array is sorted and everything after this is bad
			if (sorted().distances[i] === C_INT32_MIN) {
				break;
			}

			elements.push(
				<p
					class="search-item"
					onClick={lmb_select_file}
					data-selection-index={i}
				>
					{sorted().strings[i]} : {sorted().distances[i]}
				</p>,
			);
		}
		return <>{elements}</>;
	};

	onCleanup(() => {
		// re-assigning `ref_list` so ESLint doesn't auto-change it to const,
		// as it needs to be mutable for `ref={ref_list}` to work
		ref_list = undefined;
		cleanup_malloc();
	});

	return (
		<>
			<p>first file in current input files: {props.files()[0]}</p>

			<form onSubmit={(ref: Event) => {
				ref.preventDefault();
				// go to current arrow-key based selection
				goto_selection(sorted().strings[selection_cursor()]);
			}}>
				<input
					name="query"
					placeholder="file search"
					autocomplete="off"
					onInput={search_input}
					onkeydown={select_list_keydown}
					value={file_search_term()}
					onFocusIn={() => {
						// UX: Open search (on click or tab in)
						set_search_open(true);
					}}
				/>
				{/* type="button" as this button shouldn't submit */}
				<button type="button" onClick={() => set_search_open(!search_open())}>
					{search_open() ? "close" : "open"} results
				</button>
			</form>
			{search_open() ?
				<div ref={ref_list} class="search-list">
					{render_sorted()}
				</div> :
				""}
		</>
	);
};
