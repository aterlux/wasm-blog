import "./main.css";

import {createSignal, JSXElement} from "solid-js";

import {tc_module} from "../../out-emcc/wasm";
import {t_sig_settings} from "../util/types";
import Search from "./search";
import Settings from "./settings";
import Test from "./test";
import {test_query_strings} from "./test_strings";

export default (props: { c: tc_module }): JSXElement => {
	const [settings_open, set_settings_open] = createSignal(false);

	const [current_file, set_current_file] = createSignal("");
	const [files, set_files] = createSignal<string[]>([]);

	const [settings, set_settings] = createSignal<t_sig_settings>({
		index_file: localStorage.getItem("settings.index_file") || "/my_cdn/sample-markdown/list.md",

		// Index file will be used as source of markdown files to read.
		// If it's not provided, we'll attempt to look for references to `.md` files
		// in the HTML that the web server responds with.
		// Need the string as bool. if nothing set, default to false
		use_index_file: (localStorage.getItem("settings.use_index_file") === "true" ? true : false) || false,

		markdown_location: localStorage.getItem("settings.markdown_location") || "/my_cdn/sample-markdown",
	});

	set_files(test_query_strings);
	// setTimeout(() => {
	// 	console.warn("changing files");
	// 	set_files([
	// 		"nyani",
	// 		"neko",
	// 		"cute",
	// 		"qute",
	// 		"query",
	// 		"uuwuwuw",
	// 		"cat",
	// 		"search",
	// 		"my",
	// 		"strings",
	// 		"programming",
	// 		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non orci in sem pretium egestas.",
	// 	]);
	// }, 60000);

	return (
		<div class="main">

			<Settings
				c={props.c}
				files={files}
				set_files={set_files}
				settings={settings}
				set_settings={set_settings}
				settings_open={settings_open}
				set_settings_open={set_settings_open} />

			<div class="top-bar">
				<h1>wasm-blog</h1>
				<p>current file: {current_file() || "none"}</p>
				<button class="top-bar-settings" onclick={() => set_settings_open(true)}>settings</button>
			</div>


			<Test c={props.c} />
			<Search c={props.c} files={files} set_current_file={set_current_file} />

		</div>
	);
};
