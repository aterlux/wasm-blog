import {JSXElement} from "solid-js";

import {tc_module} from "../../out-emcc/wasm";
import {alloc_copy_strarr, free_strarr} from "../util/mem-interop";

const js_sum = (arr: number[]): number => {
	let sum = 0;
	for (let lol = 0; lol !== 10000; ++lol) {
		for (let i = 0; i < arr.length; ++i) {
			for (let j = 0; j < arr.length; ++j) {
				for (let k = 0; k < arr.length; ++k) {
					sum += arr[i];
				}
			}
		}
	}
	return sum;
};

const js_sum2 = (num1: number, num2: number): number => {
	let sum = 0;
	for (let lol = 0; lol !== 1000; ++lol) {
		sum += num1 + num2;
	}
	return sum;
};

const parseme = (c: tc_module) => {
	c._parseme();
};

const counter_js = (c: tc_module) => {
	console.log(c._fake_add(2, 1));

	const js_timer_start1 = performance.now();
	for (let i = 0; i !== 1000; ++i) {
		const js_arr = [1, 2, 3, 4, 5];
		js_sum(js_arr);
	}
	const js_timer_end1 = performance.now();
	console.log(`JS time1: ${(js_timer_end1 - js_timer_start1) / 1000}`);

	const js_timer_start2 = performance.now();
	for (let i = 0; i !== 1000; ++i) {
		js_sum2(1, 2);
	}
	const js_timer_end2 = performance.now();
	console.log(`JS time2: ${(js_timer_end2 - js_timer_start2) / 1000}`);
};

const counter_c = (c: tc_module) => {
	const c_time_start1 = performance.now();
	for (let i = 0; i !== 1000; ++i) {
		const js_arr = new Uint8Array([1, 2, 3, 4, 5]);
		const buf = c._z_malloc(js_arr.length * js_arr.BYTES_PER_ELEMENT);
		c.HEAPU8.set(js_arr, buf as number);
		c._sum(buf, js_arr.length);
		c._z_free(buf);
	}

	const c_time_end1 = performance.now();
	console.log(`C time1: ${(c_time_end1 - c_time_start1) / 1000}`);

	const c_time_start2 = performance.now();
	for (let i = 0; i !== 1000; ++i) {
		c._sum2(1, 2);
	}
	const c_time_end2 = performance.now();
	console.log(`C time2: ${(c_time_end2 - c_time_start2) / 1000}`);
};


export default (props: { c: tc_module }): JSXElement => {
	return (
		<div>
			<button onClick={() => parseme(props.c)}>call parse</button>
			<button onclick={() => counter_js(props.c)}>JS</button>
			<button onClick={() => counter_c(props.c)}>C</button>
		</div>
	);
};
