import "./settings.css";

import {Accessor, createEffect, createSignal, JSXElement, Setter} from "solid-js";

import {tc_module} from "../../out-emcc/wasm";
import {t_sig_settings} from "../util/types";



export default (props: {
	c: tc_module,
	settings: Accessor<t_sig_settings>,
	set_settings: Setter<t_sig_settings>
	files: Accessor<string[]>,
	set_files: Setter<string[]>,
	settings_open: Accessor<boolean>,
	set_settings_open: Setter<boolean>
}): JSXElement => {
	// Fugly.
	const [tmp_index_file, set_tmp_index_file] = createSignal(props.settings().index_file);
	const [tmp_use_index_file, set_tmp_use_index_file] = createSignal(props.settings().use_index_file);
	const [tmp_markdown_location, set_tmp_markdown_location] = createSignal(props.settings().markdown_location);

	// Whenever settings closed, reset temp values
	createEffect(() => {
		if (props.settings_open() === false) {
			set_tmp_index_file(props.settings().index_file);
			set_tmp_markdown_location(props.settings().markdown_location);
			set_tmp_use_index_file(props.settings().use_index_file);
		}
	});



	const save_settings = () => {
		console.log(tmp_index_file());
		console.log(tmp_use_index_file());

		props.set_settings({
			index_file: tmp_index_file(),
			use_index_file: tmp_use_index_file(),
			markdown_location: tmp_markdown_location(),
		});

		localStorage.setItem("settings.use_index_file", tmp_use_index_file() ? "true" : "false");
		localStorage.setItem("settings.index_file", tmp_index_file());
		localStorage.setItem("settings.markdown_location", tmp_markdown_location());
		// 	// const res = fetch(index_file())
		// 	// 	.then((response) => response.text())
		// 	// 	.then((data) => console.log(data));
	};

	return (
		<div
			class="settings-wrapper"
			style={props.settings_open() ? "display: flex" : "display: none"}
		>
			<div class="settings">
				<div class="close-button close-button-in-parent" onclick={() => props.set_settings_open(false)}></div>

				<h2>settings</h2>


				<form onSubmit={(ref: Event) => {
					ref.preventDefault();
				}}>


					<p class="tooltip">
						Use index file
						<span class="tooltip-text">If no index file is provided, we'll attempt to look for <code>.md</code> files in the HTML your web server sends.</span>
					</p>
					<div style="display: flex; align-items: center;">
						<input
							name="uri_index"
							placeholder="https://"
							onkeydown={(event) => {
								set_tmp_index_file(event.currentTarget.value);
							}}
							value={tmp_index_file()}
							disabled={!tmp_use_index_file()}
						/>
						<label class="toggle-switch">
							<input
								type="checkbox"
								checked={tmp_use_index_file()}
								onclick={() => set_tmp_use_index_file(!tmp_use_index_file())}
							/>
							<span class="slider"></span>
						</label>
					</div>

					<p class="tooltip">
						Markdown location
						<span class="tooltip-text">Where your markdown files are stored</span>
					</p>
					<div>
						<input
							name="uri_markdown"
							placeholder="https://"
							onkeydown={(event) => {
								set_tmp_markdown_location(event.currentTarget.value);
							}}
							value={tmp_markdown_location()}
						/>
					</div>

					<button type="button" onClick={() => save_settings()}>save</button>

				</form>


			</div>
		</div>
	);
};
