module.exports = {
	// This file is used because certain TS rules make ESLint
	// complain about there not being a parser for it.
	// All TS rules that don't make ESLint complain still
	// go into .eslint-global-config.cjs
	"rules": {

		// Make sure promise exceptions are handled
		// https://typescript-eslint.io/rules/no-floating-promises/
		"@typescript-eslint/no-floating-promises": "error",
		// https://typescript-eslint.io/rules/no-misused-promises
		"@typescript-eslint/no-misused-promises": "error",


		"@typescript-eslint/naming-convention": [
			"error",
			{ "selector": "default", "format": ["snake_case"] },
			// camelCase only because I CBA with adding exceptions for libraries
			{ "selector": "function", "format": ["snake_case", "camelCase"] },
			{ "selector": "objectLiteralMethod", "format": ["snake_case", "camelCase"] },
			// PascalCase for SolidJS components
			{ "selector": "variable", "format": ["snake_case", "PascalCase"] },

			// Allow properties on objects to be any case if they are a string,
			// and allow bare numbers. E.g., { 1: "something" };
			// See: https://typescript-eslint.io/rules/naming-convention/#ignore-properties-that-require-quotes
			{ "selector": "objectLiteralProperty", "format": null, "modifiers": ["requiresQuotes"] },
			// Also allows camelCase because other libs use it
			{ "selector": "objectLiteralProperty", "format": ["snake_case", "camelCase"] },
			// Thus:
			// const asdf = {
			// 	1: "a", // valid
			// 	2: "a", // valid
			// 	someThing: "a", // valid
			// 	some_other_thing: "a", // valid
			// 	PascalCase: "a", // invalid
			// };
			//
			// const qwer = {
			// 	"1": "a", // valid
			// 	"2": "a", // valid
			// };

			// In case libs export as camelCase or PascalCase
			{ "selector": "import", "format": ["snake_case", "camelCase", "PascalCase"] },
			// Types must be prefixed with t_
			{ "selector": "typeLike", "format": ["snake_case"], "prefix": ["t_", "tc_"] },
			// Template types must be prefixed with T_
			{ "selector": "typeParameter", "format": ["snake_case"], "prefix": ["T_"] }
		],
	}
}
