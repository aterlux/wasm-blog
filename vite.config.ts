import {exec} from "child_process";
import {existsSync} from "fs";
import {defineConfig, loadEnv, UserConfigExport} from "vite";
import solidPlugin from "vite-plugin-solid";

const regex_c_files = /.*src-c\/.*[.c|.h]/;

const print_time = () => {
	const now = new Date();
	return now.toLocaleTimeString("en-GB", {hour12: false});
};

const exec_run_wasm = (emcc: "release" | "debug", called_by: "build" | "watch") => {
	console.log(`[${print_time()}]: emcc async building wasm (${emcc}, ${called_by})`);
	// Passing _any_ string as arg compiles in release mode
	const exec_arg = emcc === "release" ? "release" : "";
	const cmd = exec(`./run_wasm.sh ${exec_arg}`);
	cmd.stdout?.on("data", (data) => console.log(data));
	cmd.stderr?.on("data", (data) => console.error(data));
};

export default ({mode}: { mode: string }): UserConfigExport => {
	process.env = {...process.env, ...loadEnv(mode, process.cwd())};

	return defineConfig({
		// esbuild: {
		// 	drop: ["console", "debugger"],
		// },
		base: process.env.VITE_ENV === "prod" ? process.env.VITE_HOSTED_URL : undefined,

		// Copy `./public/main.wasm` to `./dist/main.wasm` on build
		// assetsInclude: ["public/main.wasm"],

		server: {
			// Somehow, by some magic, this proxy makes cookies work on localhost
			proxy: {
				"/my_cdn": {
					target: "https://aterlux.com/temp_cdn",
					// eslint-disable-next-line
					changeOrigin: true,
					rewrite: (path) => path.replace(/^\/my_cdn/, ""),
				},
			},

		},

		plugins: [
			solidPlugin(),
			{
				name: "build_wasm",

				// Vite won't refresh if .wasm changes,
				// so handle all of the "what file updated" logic here
				handleHotUpdate: (info) => {
					// emcc touches the .wasm file multiple times...
					// Once .wasm is done, emcc updates the .js file,
					// and then the full-reload is triggered.
					// This just gives the dev some feedback.
					if (
						info.file.endsWith(".wasm") ||
						info.file.endsWith(".c") ||
						info.file.endsWith(".h")
					) {
						info.server.hot.send("my:wasm_wait");
					}

					// Regular file updates (or wasm.js)
					else if (
						info.file.endsWith(".ts") ||
						info.file.endsWith(".tsx") ||
						info.file.endsWith(".js") ||
						info.file.endsWith(".css") ||
						info.file.endsWith(".html")
					) {
						info.server.hot.send({type: "full-reload"});
						console.log(`[${print_time()}]: triggering full-reload`);
					}

					return [];
				},

				watchChange: (changed_file: string) => {
					// Only compile if C files change or main.wasm doesn't exist
					if (existsSync("out-emcc/wasm.wasm") && !changed_file.match(regex_c_files)) {
						return;
					}
					// Ignore changes to `compile_commands.json`, else emcc will loop a stupid amount of times
					if (changed_file.match("compile_commands.json")) {
						return;
					}

					// const time_start = performance.now();
					exec_run_wasm("debug", "watch");
					// const time_end = performance.now();
					// console.log(`emcc built wasm, took ${(time_end - time_start)/1000}`);
				},

				buildStart: (info) => {
					// const time_start = performance.now();

					// buildStart is called on `> vite` and `> vite build`,
					// aka `> npm run dev` and `npm run build`.
					// The param is `{}` if it's called on dev.
					if (Object.keys(info).length === 0) {
						exec_run_wasm("debug", "build");
					}

					else {
						exec_run_wasm("release", "build");
					}

					// const time_end = performance.now();
					// console.log(`emcc built wasm, took ${(time_end - time_start)/1000}`);
				},

			},
		],
	});
};
