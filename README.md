# wasm-blog
This is a WIP frontend-only markdown library.

By library I mean like _bibliothēca_ or _biblioteka_, as in "chest for books", not like "libsvtav1".



## Technical details
I wasn't going to use Emscripten, but it's more convenient to get their standard libraries. I am trying to minimise their JavaScript bindings/glue code.

For some reason, using `emcc` also made the C faster. Much faster. It went from being -0.5x perf, to ~3x or more in Firefox, and in one test it went from being ~0.1x in Chrome to about ~1.2x.

This ~3x perf is also with _pessimistic_ code; simple loops with "not many" iterations in the implementation function, with a lot of iterations at the JS caller. This also includes the overhead of calling `c.malloc()`/`c.free()` _in the JS loop_.

A hyper-optimistic scenario, with 10 JS calls, and 10000000 iterations in the impl, has ~8x perf in C.



## Developing
Run: `> npm run start`
- Website hosted locally
- C may be **_slow_**
- Hot-reloading when HTML/CSS/TS update
- Hot-reloading when C updates


When you save a `.c` or `.h` file, or when the compiler changes the `.wasm` file, the body background will change. When you're in this state, any executed WASM is potentially outdated until the page auto-refreshes.


Do not edit files in `./out-emcc/`

Be aware of potential unsafety and UB, see the compiler rules section in



## Compiling/Building
Run: `> npm run build`
- `./dist/` is produced
- C is optimised
- HTML/CSS/JS is bundled and minified


Notes:
- I've spotted the WASM and JS files being inflated at times when using `./run_publish.sh`. I don't know why that is, but currently the `.wasm` file should _not_ be 500KiB.
- It might be worth using `-Os` or `-Oz` instead of `-O2` or `-O3`. However, this requires benchmarking.


### Clang/emcc compiler rules
Read:
- [clang manual](https://clang.llvm.org/docs/UsersManual.html)
- [emscripten "compiler settings"](https://emscripten.org/docs/tools_reference/settings_reference.html) or [emscripten `settings.js`](https://github.com/emscripten-core/emscripten/blob/main/src/settings.js)
- [emscripten "optimising code"](https://emscripten.org/docs/optimizing/Optimizing-Code.html).


Main rules:
- `-O3` - seemingly produced much faster results
- `-flto` - link time optimization
- `-ffast-math` - [reference](https://clang.llvm.org/docs/UsersManual.html#cmdoption-ffast-math)
- `-funsafe-math-optimizations` - [reference](https://clang.llvm.org/docs/UsersManual.html#cmdoption-f-no-unsafe-math-optimizations)
- `-mnontrapping-fptoint` - apparently uses newer math instructions that don't trap, but might not be supported by older VMs - [reference](https://emscripten.org/docs/compiling/WebAssembly.html#trapping)
<!-- - `-fstrict-aliasing` - [reference](https://clang.llvm.org/docs/UsersManual.html#id11), [explanation](https://stackoverflow.com/a/99010/13310905) - makes having multiple pointers of "incompatible type" to one object illegal. -->
- `-sFILESYSTEM=0` - no files or streams, aside from `printf()` and `puts()` ("hackishly supported"). I don't know if my usage of `md_html()` is safe with the "hackishly supported" `printf()` - [reference](https://emscripten.org/docs/tools_reference/settings_reference.html?highlight=assertions#filesystem)
- `sMALLOC=dlmalloc` - dlmalloc is apparently faster than emmalloc, but in release build it's ~7KiB larger - [reference](https://emscripten.org/docs/optimizing/Optimizing-Code.html#allocation)
- `-sASSERTIONS=0` - no runtime assertions in JS and C (I think)
- `-sSTACK_OVERFLOW_CHECK=0` - dragons
- `-sCHECK_NULL_WRITES=0` - dragons. Apparently makes no diff if already set `-sSTACK_OVERFLOW_CHECK=0`
- `-sSAFE_HEAP=0` - no checks against "segfaults"



## Requirements
System (necessary):
- to be on a system that executes bash scripts
- `clang`
- `llvm`
- `lld`
- `emcc` (emscripten, located in `/lib/emscripten/emcc` on arch)
- `nodejs` (I suggest using `nvm`)
- `npm`


System (desirable):
- `clang-format`
- `find`
- `dos2unix`



# C Libraries
In use:
- md4c - Fast Markdown parser - https://github.com/mity/md4c
- - `<limits.h>`
- - `<stdint.h>`
- - `<stdio.h>`
- - `<stdlib.h>`
- - `<string.h>`


Used previously:
- walloc - `malloc()` etc. for WASM - https://github.com/wingo/walloc
- - I used this when I wanted to not use Emscripten and use `-nostdlib`, but I caved in as I expected I would have to write or find several other stdlib implementations and potentially make changes to md4c.


Others:
- markdown-wasm - md4c WASM implementation - https://github.com/rsms/markdown-wasm
- - I referred to this when trying to get md4c initially working. It took me _hours_ and I ended up switching to [mmd](https://github.com/michaelrsweet/mmd) temporarily to get _any_ sort of output, before I finally got a barebones working output from md4c.



## Code details
TS types are prefixed with `t_`, TS types used for C in some way are prefixed with `tc_`.



## References



### JavaScript stuff
- https://github.com/farzher/fuzzysort/blob/master/fuzzysort.js#L40
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Typed_arrays



### Memory
Keep in mind:
- JavaScript is big endian, WASM is little endian
- - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DataView/setInt16
- struct memory padding -- `-Wpadded` is used to try to help with this
- - https://levelup.gitconnected.com/how-struct-memory-alignment-works-in-c-3ee897697236


### Perf
- https://takahirox.github.io/WebAssembly-benchmark/



### Libs
- https://github.com/wingo/walloc
- https://github.com/rsms/markdown-wasm
- https://github.com/mity/md4c
- https://www.msweet.org/mmd/mmd.html
- https://github.com/muayyad-alsadi/malloc0
- https://github.com/sheredom/utest.h



### WASM
- https://webassembly.org/getting-started/developers-guide/
- - https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm
- - https://developer.mozilla.org/en-US/docs/WebAssembly/existing_C_to_wasm
- https://emscripten.org/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html?highlight=directly#interacting-with-code-direct-function-calls
- https://emscripten.org/docs/api_reference/preamble.js.html?highlight=stringtoutf8#stringToUTF8
- https://v8.dev/blog/emscripten-standalone-wasm
- https://surma.dev/things/c-to-webassembly/
- https://schellcode.github.io/webassembly-without-emscripten
- - https://schellcode.github.io/wajic-how-and-why
- - https://gist.github.com/kripken/cbffda150b4e1583bdad832d070944da
- https://00f.net/2019/04/07/compiling-to-webassembly-with-llvm-and-clang/
- https://surma.dev/things/c-to-webassembly/
- https://schellcode.github.io/webassembly-without-emscripten
- https://stackoverflow.com/questions/61795187/webassembly-return-string-use-c
- https://stackoverflow.com/questions/45295339/can-i-somehow-build-webassembly-code-without-the-emscripten-glue
- https://github.com/schellingb/ClangWasm?tab=readme-ov-file
- https://stackoverflow.com/questions/59587066/no-emscripten-how-to-compile-c-with-standard-library-to-webassembly
- https://github.com/WebAssembly/wasi-sdk
- Someone also trying to use md4c lib: https://stackoverflow.com/questions/77113189/passing-c-style-strings-into-a-c-library-in-c-md4c
- https://livebook.manning.com/book/webassembly-in-action/c-emscripten-macros/v-7/58



### Algorithms
- https://www.reddit.com/r/cpp/comments/fwhuk0/approximate_string_match_library_in_c/
- https://github.com/addaleax/levenshtein-sse/blob/master/levenshtein-sse.hpp
- https://github.com/guilhermeagostinelli/levenshtein
- https://mx.hehe.si/articles/speeding-up-levenshtein.html
- https://en.wikipedia.org/wiki/Approximate_string_matching
- https://stackoverflow.com/questions/4057513/levenshtein-distance-algorithm-better-than-onm
- - http://blog.notdot.net/2010/07/Damn-Cool-Algorithms-Levenshtein-Automata

