#!/bin/bash

# # https://stackoverflow.com/a/99010/13310905
#
# NOTE: Temporarily disabling -Woverlength-strings

exported_runtime_funcs="['stringToUTF8', 'lengthBytesUTF8', 'UTF8ToString', 'setValue']"
module_attributes="print,printErr"
# NOTE: For some reason, when using dlmalloc, I could still access data after free() ...??????
malloc_lib="emmalloc"
initial_memory=1073741824
stack_size=8388608
c_sources="
./src-c/lib/entity.c
./src-c/lib/md4c.c
./src-c/lib/md4c-html.c
./src-c/levenshtein.c
./src-c/fuzzy_match.c
./src-c/random.c
./src-c/main.c
"

# Test:
# -sUSE_CLOSURE_COMPILER=1 \

# If an arg was given, use release mode
if [ ! -z "$1" ]; then
	/lib/emscripten/emcc -O3 \
		-std=c11 \
		-Wall \
		-Wextra \
		-Wpedantic \
		-Wpadded \
		-Wno-overlength-strings \
		-flto \
		-ffast-math \
		-funsafe-math-optimizations \
		-mnontrapping-fptoint \
		-sMINIFY_HTML=0 \
		-sENVIRONMENT=web \
		-sEXPORT_ES6=1 \
		-sEXPORTED_RUNTIME_METHODS="$exported_runtime_funcs" \
		-sINCOMING_MODULE_JS_API=$module_attributes \
		-sFILESYSTEM=0 \
		-sMALLOC=$malloc_lib \
		-sINITIAL_MEMORY=$initial_memory \
		-sSTACK_SIZE=$stack_size \
		-sASSERTIONS=0 \
		-sSTACK_OVERFLOW_CHECK=0 \
		-sCHECK_NULL_WRITES=0 \
		-sSAFE_HEAP=0 \
		-sEXPORT_NAME=c_module \
		$c_sources \
		-o ./out-emcc/wasm.js \
		-MJ compile_commands.json
else
	# sASSERTIONS=2 is _insanely_ slow
	# -sSYSCALL_DEBUG=1 \
	/lib/emscripten/emcc -O2 \
		-std=c11 \
		-Wall \
		-Wextra \
		-Wpedantic \
		-Wpadded \
		-Wno-overlength-strings \
		-fsanitize=address \
		-flto \
		-sMINIFY_HTML=0 \
		-sENVIRONMENT=web \
		-sEXPORT_ES6=1 \
		-sEXPORTED_RUNTIME_METHODS="$exported_runtime_funcs" \
		-sINCOMING_MODULE_JS_API=$module_attributes \
		-sFILESYSTEM=0 \
		-sMALLOC=$malloc_lib \
		-sINITIAL_MEMORY=$initial_memory \
		-sSTACK_SIZE=$stack_size \
		-sASSERTIONS=1 \
		-sSTACK_OVERFLOW_CHECK=1 \
		-sCHECK_NULL_WRITES=1 \
		-sSAFE_HEAP=0 \
		-sEXPORT_NAME=c_module \
		$c_sources \
		-o ./out-emcc/wasm.js \
		-MJ compile_commands.json
fi
